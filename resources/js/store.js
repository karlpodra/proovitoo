import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
        userData: []
    },
    mutations: {
        addData (state, data) {
            state.userData = data
        }
    }
})
export default store
