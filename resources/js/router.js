import VueRouter from 'vue-router'
import Register from './pages/Register'
import UserList from './pages/UserList'
const routes = [
    {
        path: '/',
        name: 'register',
        component: Register,
    },
    {
        path: '/list',
        name: 'list',
        component: UserList,
    }
]
const router = new VueRouter({
    history: true,
    mode: 'history',
    routes,
})
export default router
